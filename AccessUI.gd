extends PanelContainer

const default_theme = preload("res://DefaultTheme.tres")

signal set_font_scale
signal set_font_color
signal set_head_bob


# debug
func _ready() -> void:
  if not theme:
    theme = default_theme.duplicate(true)
  prints(theme)

  assert(OK == connect("set_font_scale", self, "_on_set_font_scale"))
  assert(OK == connect("set_font_color", self, "_on_set_font_color"))
  assert(OK == connect("set_head_bob", self, "_on_set_head_bob"))

  assert(OK == $VBoxContainer/FontScale.connect(
    "value_changed", self, "_on_set_font_scale"))
  assert(OK == $VBoxContainer/FontColor.connect(
    "value_changed", self, "_on_set_font_color"))
  assert(OK == $VBoxContainer/Headbob.connect(
    "value_changed", self, "_on_set_head_bob"))

func _on_set_font_scale(scale: float) -> void:
  prints(name, 'font scale', scale)
  var font = theme.default_font
  if font is DynamicFont:
    font.size = default_theme.default_font.size * scale

func _on_set_font_color(color: Color) -> void:
  prints(name, 'font color', color)
  theme.set_color("font_color", "label", color)

func _on_set_head_bob(amount: float) -> void:
  pass

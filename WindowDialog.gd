extends WindowDialog


func _input(event: InputEvent) -> void:
  if event.is_action("ui_cancel") && event.pressed:
    if is_visible_in_tree():
       hide()
    else:
      show()

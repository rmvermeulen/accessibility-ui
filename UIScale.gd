extends HBoxContainer

signal value_changed

export var value: float = 1 setget _set_value
onready var _initial_value = value

func _ready() -> void:
  _set_value(value)
  assert(OK == $LineEdit.connect(
    "text_changed", self, "_on_LineEdit_text_changed"))
  assert(OK == $HSlider.connect(
    "value_changed", self, "_on_HSlider_value_changed"))
  assert(OK == $ResetButton.connect(
    "pressed", self, "_reset"))

func _reset() -> void:
  _set_value(_initial_value)

func _set_value(new_value: float) -> void:
  value = new_value
  if not get_child_count():
    return

  $HSlider.value = value
  $LineEdit.text = str($HSlider.value)

  emit_signal("value_changed", value)

func _on_LineEdit_text_changed(new_text: String) -> void:
  _set_value(float(new_text))


func _on_HSlider_value_changed(new_value: float) -> void:
  _set_value(new_value)

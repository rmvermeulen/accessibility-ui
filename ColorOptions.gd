extends OptionButton

const colors = ["black", "white", "blue", "red", "green", "yellow", "purple"]

func _ready() -> void:
  for color in colors:
    add_item(color)
  connect("item_selected", self, "_on_item_selected")

func _on_item_selected(index: int) -> void:
  var color: Color
  match colors[index]:
    "black": color = Color.black
    "white": color = Color.white
    "blue": color = Color.blue
    "red": color = Color.red
    "green": color = Color.green
    "yellow": color = Color.yellow
    "purple": color = Color.purple
  get_parent().emit_signal("value_changed", color)
